package com.cleo.labs.trigger.persist.akka.events;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by sbelur on 20/09/15.
 */
public final class LexEvent implements Serializable{


    private static final long serialVersionUID = 1L;

    private String[] commandsToExecute;
    private final String[] actionAliasPath;
    private final String eventName;
    private final String[] parameters;
    private final String sessionid;
    private final String connectedNode;

    public LexEvent(String[] commandsToExecute, String[] actionAliasPath, String eventName, String[] parameters, String sessionid,String connectedNode) {
        this.commandsToExecute = commandsToExecute;
        this.actionAliasPath = actionAliasPath;
        this.eventName = eventName;
        this.parameters = parameters;
        this.sessionid = sessionid;
        this.connectedNode = connectedNode;
    }

    public String getConnectedNode() {
        return connectedNode;
    }


    public String[] getCommandsToExecute() {
        return commandsToExecute;
    }

    // For the session case,commands are mutated and set last. (Revisit immutability)
    public void setCommandsToExecute(String[] commandsToExecute) {
        this.commandsToExecute = commandsToExecute;
    }

    public String[] getActionAliasPath() {
        return actionAliasPath;
    }

    public String getEventName() {
        return eventName;
    }

    public String[] getParameters() {
        return parameters;
    }


    public String getSessionid() {
        return sessionid;
    }

    @Override
    public String toString() {
        return super.toString() + " LexEvent{" +
                "commandsToExecute=" + Arrays.toString(commandsToExecute) +
                ", actionAliasPath=" + Arrays.toString(actionAliasPath) +
                ", eventName='" + eventName + '\'' +
                ", parameters=" + Arrays.toString(parameters) +
                ", nodehost="+connectedNode +
                '}';
    }
}
