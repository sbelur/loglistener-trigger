package com.cleo.labs.trigger;

/**
 * Created by sbelur on 20/09/15.
 */
public class Logger {
    /**
     * Prints a debug {@code message} on {@code stdout}, which is routed
     * to {@code Harmonyd.out} or {@code VLTraderd.out} (or the upstart
     * log), if DEBUG is enabled.  To enable DEBUG, set {@code debug} to
     * {@code true} in the YAML configuration file.
     * @param message the message
     */
    public static void debug(String message) {
        if (Configuration.getConfiguration().getDebug()) {
            System.err.println(message);
        }
    }

    /**
     * Prints a debug stack trace on {@code stdout}, which is routed
     * to {@code Harmonyd.out} or {@code VLTraderd.out} (or the upstart
     * log), if DEBUG is enabled.  To enable DEBUG, set {@code debug} to
     * {@code true} in the YAML configuration file.
     * @param message the message
     */
    public static void debug(Throwable throwable) {
        if (Configuration.getConfiguration().getDebug()) {
            throwable.printStackTrace();
        }
    }
}
