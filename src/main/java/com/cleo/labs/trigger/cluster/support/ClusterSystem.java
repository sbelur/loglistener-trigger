package com.cleo.labs.trigger.cluster.support;

import akka.actor.*;
import akka.contrib.pattern.*;
import com.cleo.labs.trigger.Logger;
import com.cleo.labs.trigger.persist.akka.messages.ActionCommand;
import com.cleo.labs.trigger.persist.akka.messages.Discard;
import com.cleo.labs.trigger.persist.akka.persistor.LexEventPersistor;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sbelur on 22/09/15.
 */
public class ClusterSystem {

    private static final ClusterSystem INSTANCE = new ClusterSystem();

    private AtomicBoolean configured = new AtomicBoolean(false);

    private ActorRef clusterProxy;

    private ActorSystem system;

    private static int noOfTopics = 4;

    private boolean clusteredPersistorSetup = true; // TODO set this based on topology.

    private ActorRef memberListenerProxy;

    private ClusterSystem() {
    }

    public static ClusterSystem getInstance() {
        return INSTANCE;
    }


    public ActorRef getClusterProxy() {
        return clusterProxy;
    }


    public ActorRef getMemberListenerProxy() {
        return memberListenerProxy;
    }

    /***
     * <pre>
     *
     * a. Reads cluster config
     * b. Registers w/ ClusterSharding Extension
     *
     * </pre>
     */
    public void initalize() {

        if (configured.compareAndSet(false, true)) {
            Config rootConfig = ConfigFactory.load();
            system = ActorSystem.create("event-processing-cluster", rootConfig);
            noOfTopics = system.settings().config().getInt("noOfTopics");
            clusteredPersistorSetup = system.settings().config().getBoolean("clusteredPersistorSetup");
            System.out.println("Configured  " + noOfTopics + " topics in Kafka");




            if(clusteredPersistorSetup) {

                ClusterSharding clusterSharding = (ClusterSharding)ClusterSharding.apply(system);
                clusterSharding.start("LexEventPersistor", Props.create(LexEventPersistor.class, ClusterMode.ON), new LexEventShardMapper());


                system.actorOf(ClusterSingletonManager.defaultProps(
                        Props.create(PingActor.class), "pingActor",
                        // "" -> No role indication - Here we are not concerned with roles. All entities comes up in a VM.
                        PoisonPill.getInstance(), ""), "pingsingleton");


            }
            else {

               system.actorOf(ClusterSingletonManager.defaultProps(
                        Props.create(LexEventPersistor.class, ClusterMode.ON), "lexEventPersistor",
                        // "" -> No role indication - Here we are not concerned with roles. All entities comes up in a VM.
                        PoisonPill.getInstance(), ""), "singleton");

                clusterProxy = system.actorOf(ClusterSingletonProxy.defaultProps("/user/singleton/lexEventPersistor",
                        // "" -> No role indication - Here we are not concerned with roles. All entities comes up in a VM.
                        ""), "lexEventPersistorProxy");

            }

            system.actorOf(ClusterSingletonManager.defaultProps(
                    Props.create(ClusterEventListener.class), "memberListener",
                    // "" -> No role indication - Here we are not concerned with roles. All entities comes up in a VM.
                    PoisonPill.getInstance(), ""), "singletonClusterLisener");

            memberListenerProxy = system.actorOf(ClusterSingletonProxy.defaultProps("/user/singletonClusterLisener/memberListener",
                    // "" -> No role indication - Here we are not concerned with roles. All entities comes up in a VM.
                    ""), "memberListenerProxy");

            System.out.println("Started singleton actor for cluster listener");

        } else {
            Logger.debug("Cluster system already configured");
        }






    }


    public ActorSystem getActorSystem() {
        return system;
    }


    private class LexEventShardMapper implements ShardRegion.MessageExtractor {

        @Override
        public String entryId(Object message) {
            if (message instanceof ActionCommand) {
                String sessionid = ((ActionCommand) message).getSessionid();
                String entryId = "" + (sessionid.hashCode() % noOfTopics);
                return entryId;
            }
            else if(message instanceof Discard){
                int sessionid = ((Discard) message).getId();
                String entryId = "" + (sessionid % noOfTopics);
                return entryId;

            }
            return null;
        }

        @Override
        public Object entryMessage(Object message) {
            return message;
        }

        @Override
        public String shardId(Object message) {
            if (message instanceof ActionCommand) {
                String sessionid = ((ActionCommand) message).getSessionid();
                return "" + (sessionid.hashCode() % noOfTopics);
            }
            else if(message instanceof Discard){
                int sessionid = ((Discard) message).getId();
                String entryId = "" + (sessionid % noOfTopics);
                return entryId;
            }
            return null;
        }
    }


}
