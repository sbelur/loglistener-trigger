# README #

Cleo Harmony supports a Custom LexiComLogListener Class.  This example shows an implementation of LexiComLogListener that tracks FTP and SFTP user sessions and file transfers occurring during those sessions.  A configuration file provides a set of triggering
rules that allow specified <action>s to be executed on login, logout, and file transfer.

## YAML Configuration ##

### Example ##

A sample `conf/logaction.yaml` file might appear as follows:

```
threads: 10
debug: false
rules:
- event: login
  action: login
- event: logout
  action: logout
- event: file
  action: host/mailbox/action
  host: sftp users
  mailbox: ldap users
```

A sample `cluster.conf` file for AKKA is present in conf folder for cluster config. (This needs to be copied to Harmony conf)
A sample `singlevm.conf` file for AKKA is present in conf folder for non-clutser mode. (This needs to be copied to Harmony conf)

[Also additional system property `-Dconfig.file`=<full path of Harmony cond>/<conf file name> should be added]

A sample `logback.xml` file for sl4j logging configuration is present in conf folder. (This needs to be copied to Harmony conf)


For each rule, `event` (one of `login`, `logout`, or `file`) and `action` are required.  `action` may be fully qualified as `host/mailbox/action` or may be just an `action` name relative to the `host/mailbox` triggering the event.

Triggers may be restricted to specific local (s)FTP user hosts or mailboxes by adding the optional `host:` and/or `mailbox:` attributes to a rule.  If present, the action will only trigger for the associated host/mailbox (matching is case-insensitive).

Actions may contain special replacement tokens of the form `%keyword%`.  When the action is executed, these tokens will be replaced with metadata extracted from the event.  The following metadata tokens are supported:

| Event  | Token     | Description |
|--------|-----------|-------------|
| all    | %host%    | the (s)FTP user host |
|        | %mailbox% | the (s)FTP user mailbox |
| file   | %file%    | the fully qualified destination filename |
|        | %user%    | the (s)FTP username |



## Installation ##

### Step 1: Obtain JAR files ###

First, obtain the `loglistener-trigger-version.jar` (currently `loglistener-trigger-0.0.1-SNAPSHOT.jar`).  You can obtain a copy from the BitBucket Repository [Downloads](https://bitbucket.org/jthielens/loglistener-trigger/downloads) page, or build it yourself from the POM.

Additional library jars are required.  They can be obtained from Maven Central using the following dependency coordinates:

```xml

<dependency>
    <groupId>com.yammer.metrics</groupId>
    <artifactId>metrics-core</artifactId>
    <version>2.2.0</version>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>org.apache.kafka</groupId>
    <artifactId>kafka-clients</artifactId>
    <version>0.8.2.1</version>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>org.scala-lang.modules</groupId>
    <artifactId>scala-parser-combinators_2.11</artifactId>
    <version>1.0.2</version>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>org.apache.zookeeper</groupId>
    <artifactId>zookeeper</artifactId>
    <version>3.4.6</version>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>com.101tec</groupId>
    <artifactId>zkclient</artifactId>
    <version>0.3</version>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>com.github.krasserm</groupId>
    <artifactId>akka-persistence-kafka_2.11</artifactId>
    <version>0.4</version>
</dependency>
<dependency>
    <groupId>org.apache.kafka</groupId>
    <artifactId>kafka_2.11</artifactId>
    <version>0.8.2.1</version>
</dependency>

<dependency>
  <groupId>com.typesafe.akka</groupId>
  <artifactId>akka-slf4j_2.11</artifactId>
  <version>2.3.12</version>
</dependency>

<dependency>
   <groupId>ch.qos.logback</groupId>
   <artifactId>logback-classic</artifactId>
   <version>1.1.3</version>
</dependency>
<dependency>
   <groupId>ch.qos.logback</groupId>
   <artifactId>logback-core</artifactId>
   <version>1.1.3</version>
</dependency>
<dependency>
	<groupId>org.scala-lang</groupId>
	<artifactId>scala-library</artifactId>
	<version>2.11.0</version>
</dependency>
<dependency>
	<groupId>com.typesafe</groupId>
	<artifactId>config</artifactId>
	<version>1.2.1</version>
</dependency>
<dependency>
	<groupId>com.google.protobuf</groupId>
	<artifactId>protobuf-java</artifactId>
	<version>2.5.0</version>
</dependency>
<dependency>
   <groupId>org.iq80.leveldb</groupId>
   <artifactId>leveldb</artifactId>
   <version>0.7</version>
</dependency>
<dependency>
   <groupId>org.fusesource.leveldbjni</groupId>
   <artifactId>leveldbjni-all</artifactId>
   <version>1.8</version>
</dependency>
<dependency>
    <groupId>com.typesafe.akka</groupId>
    <artifactId>akka-actor_2.11</artifactId>
    <version>2.3-SNAPSHOT</version>
</dependency>
<dependency>
    <groupId>com.typesafe.akka</groupId>
    <artifactId>akka-persistence-experimental_2.11</artifactId>
    <version>2.3.12</version>
</dependency>
<dependency>
  <groupId>de.odysseus.juel</groupId>
  <artifactId>juel-api</artifactId>
  <version>2.2.7</version>
</dependency>
<dependency>
  <groupId>de.odysseus.juel</groupId>
  <artifactId>juel-impl</artifactId>
  <version>2.2.7</version>
</dependency>
<dependency>
  <groupId>de.odysseus.juel</groupId>
  <artifactId>juel-spi</artifactId>
  <version>2.2.7</version>
</dependency>
<dependency>
  <groupId>org.yaml</groupId>
  <artifactId>snakeyaml</artifactId>
  <version>1.14</version>
</dependency>
```

or from the following links: [Some of these jars are ONLY needed for Kafka integration - these are mentioned below]

[metrics core](http://central.maven.org/maven2/com/yammer/metrics/metrics-core/2.2.0/metrics-core-2.2.0.jar) - Or from KAFKA distribution in libs

[kafka-clients](http://central.maven.org/maven2/org/apache/kafka/kafka-clients/0.8.2.1/kafka-clients-0.8.2.1.jar)  - Or from KAFKA distribution in libs

[scala parser](http://central.maven.org/maven2/org/scala-lang/modules/scala-parser-combinators_2.11/1.0.2/scala-parser-combinators_2.11-1.0.2.jar)  - Or from KAFKA distribution in libs

[zookeeper](http://central.maven.org/maven2/org/apache/zookeeper/zookeeper/3.4.6/zookeeper-3.4.6.jar)  - Or from KAFKA distribution in libs

[zkclient](http://central.maven.org/maven2/com/101tec/zkclient/0.3/zkclient-0.3.jar) - Or from KAFKA distribution in libs

[akka kafka persistence](http://dl.bintray.com/krasserm/maven/com/github/krasserm/akka-persistence-kafka_2.11/0.4/akka-persistence-kafka_2.11-0.4.jar) - Needed for KAFKA integration

[kafka] (http://mvnrepository.com/artifact/org.apache.kafka/kafka_2.11/0.8.2.1)  - Or from KAFKA distribution in libs

[akka slfj](http://central.maven.org/maven2/com/typesafe/akka/akka-slf4j_2.11/2.3.12/akka-slf4j_2.11-2.3.12.jar)

[logback classic](http://central.maven.org/maven2/ch/qos/logback/logback-classic/1.1.3/logback-classic-1.1.3.jar)

[logback core](http://central.maven.org/maven2/ch/qos/logback/logback-core/1.1.3/logback-core-1.1.3.jar)

[scala library](http://central.maven.org/maven2/org/scala-lang/scala-library/2.11.0/scala-library-2.11.0.jar)

[protobuf](http://central.maven.org/maven2/com/google/protobuf/protobuf-java/2.5.0/protobuf-java-2.5.0.jar)

[akka-config](http://central.maven.org/maven2/com/typesafe/config/1.2.1/config-1.2.1.jar)

[akka-persistence](http://central.maven.org/maven2/com/typesafe/akka/akka-persistence-experimental_2.11/2.3.12/akka-persistence-experimental_2.11-2.3.12.jar)

[akka-actor](http://repo.akka.io/snapshots/com/typesafe/akka/akka-actor_2.11/2.3-SNAPSHOT/akka-actor_2.11-2.3-SNAPSHOT.jar)

[leveldb](http://central.maven.org/maven2/org/iq80/leveldb/leveldb/0.7/leveldb-0.7.jar)

[leveldb-jni](http://central.maven.org/maven2/org/fusesource/leveldbjni/leveldbjni-all/1.8/leveldbjni-all-1.8.jar)

[leveldb-api](http://central.maven.org/maven2/org/iq80/leveldb/leveldb-api/0.7/leveldb-api-0.7.jar)

[juel-api](http://central.maven.org/maven2/de/odysseus/juel/juel-api/2.2.7/juel-api-2.2.7.jar)

[juel-impl](http://central.maven.org/maven2/de/odysseus/juel/juel-impl/2.2.7/juel-impl-2.2.7.jar)

[juel-spi](http://central.maven.org/maven2/de/odysseus/juel/juel-spi/2.2.7/juel-spi-2.2.7.jar)

[snakeyaml](http://central.maven.org/maven2/org/yaml/snakeyaml/1.14/snakeyaml-1.14.jar)  

The `loglistener-trigger-version.jar` must be copied into `lib/api`.  The remaining JAR files should be installed into `lib/uri`, as they are also used by several URI plugins.

For Persistence support, additional jars are needed. The following are the jars to be copied into `lib/api`.

`protobuf-java-2.5.0.jar`  - For Serialization support

`config-1.2.1.jar` - For configuring AKKA

`scala-library-2.11.0.jar`-  For the APIs used by AKKA internally

`akka-persistence-experimental_2.11-2.3.12.jar` - For persistence support (experimental as production version needs JAVA 8)

`akka-actor_2.11-2.3-SNAPSHOT.jar` - Actor library for parallel execution, resilience support

`leveldb-0.7.jar`  - For persistence store

`leveldb-api-0.7.jar`

`leveldbjni-all-1.8.jar` - Java port of main Level DB distribution.



### Step 2: Configure system.properties ###

While Cleo Harmony/VLTrader will load the LogListener class from `lib/api` JAR automatically, this JAR and the dependent JARs must also be added to the classpath.  This is controlled by the `cleo.additional.classpath` property in `conf/system.properties`, which should look something like this:

```ini
cleo.additional.classpath=﻿logconf:lib/api/metrics-core-2.2.0.jar:lib/api/kafka-clients-0.8.2.1.jar:lib/api/scala-parser-combinators_2.11-1.0.2.jar:lib/api/zookeeper-3.4.6.jar:lib/api/zkclient-0.3.jar:lib/api/kafka_2.11-0.8.2.1.jar:lib/api/akka-persistence-kafka_2.11-0.4.jar:lib/api/akka-slf4j_2.11-2.3.12.jar:lib/api/logback-core-1.1.3.jar:lib/api/logback-classic-1.1.3.jar:lib/api/protobuf-java-2.5.0.jar:lib/api/config-1.2.1.jar:lib/api/scala-library-2.11.0.jar:lib/api/akka-persistence-experimental_2.11-2.3.12.jar:lib/api/akka-actor_2.11-2.3-SNAPSHOT.jar:lib/api/leveldb-0.7.jar:lib/api/leveldb-api-0.7.jar:lib/api/leveldbjni-all-1.8.jar:lib/api/loglistener-trigger-0.0.1-SNAPSHOT.jar:lib/uri/juel-api-2.2.7.jar:lib/uri/juel-impl-2.2.7.jar:lib/uri/juel-spi-2.2.7.jar:lib/uri/snakeyaml-1.14.jar
```

Take care when editing `cleo.additional.classpath` as other JAR files may be listed there for other purposes.

Note: Here ﻿`logconf` is folder where logback.xml is placed. This is for sl4j logging. If not present, logs go to STDOUT.


### Step 3: Register the Custom Log Listener ###

From the Harmony/VLTrader admin UI, set the `Custom LexiComLogListener Class` on the `Other` panel under `Options` to the fully-qualified listener class name:

```
com.cleo.labs.trigger.PersistenceBackedEventTrigger
```

The above should be the choice of listener if event persistence is needed in single VM - The other option is to use


```
com.cleo.labs.trigger.cluster.ClusterBasedPersistenceBackedEventTrigger
```

The above should be the choice of listener if event persistence is needed in cluster setup - The other option is to use


```
com.cleo.labs.trigger.InMemoryBackedEventTrigger
```

This class should be used for in memory event handling.

If the JAR file has been properly installed in `lib/api`, the `Check` button should confirm that the class can be properly loaded.

You must stop and restart Harmony/VLTrader before you can actually use the LogListener class.

### Step 4: Test it Out ###

To see the class in action, create a local FTP or SFTP user and add a `logout` action (its name will display as `<logout>` in the UI, following the Harmony `<action>mailbox@host` naming convention).  Do not schedule the action--instead, login and logout from an FTP or SFTP client and observe that the action runs.

You may enable debug messages by setting `debug: true` in the `logaction.yaml` file located in the Harmony `conf` directory.  If enabled, debug messages will be written to `stdout`, which is typically sent to `Harmonyd.out` (or to `/var/log/upstart/servicename` if you are running Harmony under upstart).

The triggered actions run in a dedicated thread pool.  By default this pool is configured for 10 threads and triggered actions beyond the pool size will queue until a thread is available.  The thread pool size can be adjusted by setting `threads: n` in the `logaction.yaml` file.

Changes to `logaction.yaml` are processed without restarting Harmony and the rules and other settings will be updated on-the-fly.  The listener checks the YAML file for an updated timestamp every 10 seconds when events are processed.


For testing the `session support` as of now, have a ftp user as batch_ftp in Harmony and login using the same.

For testing `Kafka persistence` , please follow these steps

a. Install ﻿kafka_2.11-0.8.2.1 in the machine(s) where you intend to run it.

b. For this setup, run a single node

c. Edit the following property files (conf folder of kafka)

   i.   consumer.properties ( zookeeper.connect=<this_host_ip>:2181)

   ii.  producer.properties ( metadata.broker.list=<this_host_ip>:9092)

   iii. server.properties - Following properties

         a. (﻿host.name=<this_host_ip>)

         b. (﻿zookeeper.connect=<this_host_ip>:2181)

         c. (﻿log.dirs=<path of kafka logs)

   iv.  zookeeper.properties (﻿properties:dataDir=<zkconfig dir path>)

d. For running Kafka , execute the following command

   i.   cd <kafka_install_path>

   ii.  bin/zookeeper-server-start.sh config/zookeeper.properties & \

   iii. bin/kafka-server-start.sh config/server.properties & \

e. In Harmonyc.lax set the value of -Dconfig.file to cluster.conf file [present in conf folder]

f. Edit Harmonyc.lax for the following properties

   i. <zkhost> as the ip of m/c where zookeeper is running - here its same as kafka machine

   ii. seed-nodes section should list the <localip> as ip of of the Harmony servers for eg. and <otherip> as ip of the other Harmony IP.

   (Basically seed nodes should be in SAME order in both of machines. ALSO the first seed ip is generally the ONE where you would start first)

   iii. hostname under netty.tcp  section should be set to Harmony server IP where its running.

g. Right now, session.force.timeout.in.seconds value is set to 20 seconds in cluster.conf . Meaning , after the first file is received

   a logout event is forced so as not to put any pressure on memory. This is application for session based testing. Please keep in mind that

   right now, AKKA will redeliver event if an ACK is not received for the event within 1 minute from the worker. ALSO right now this 1 minute

   of retry interval is hard-coded. This needs to be pulled out eventually in .conf files.


## Extending the LogListener ##


This repository includes an Maven POM that can be used to modify and/or build the code.  You will need also:

* an installed copy of Cleo VLTrader or Cleo Harmony
* access to the Cleo Nexus repository, to satisfy the dependencies of the Maven project, or
* a copy of the `LexiCom.jar` and `lexbean.jar` files copied from the `lib` directory of your Harmony installation
  and manually placed into your Maven cache.

This project depends on Java 7, which is supported by Cleo Harmony/VLTrader 5.1 and later.

## TODO ##

The `file` event, based on the `<File>` element in the XML log, needs to be refined into at least five separate sub-events capturing the following use cases:

| Event    | Description                                 | Direction    |
|----------|---------------------------------------------|--------------|
| upload   | local user uploads a file                   | Host->Local  |
| download | local user downloads a file                 | Local->Host  |
| push     | a file is sent to a remote host (PUT)       | Local->Host  |
| pull     | a file is received from a remote host (GET) | Host->Local  |
| copy     | a file is copied locally (LCOPY)            | Local->Local |

Need to check memory impact of file events kept track for a session and any optimizations that can be done for that.

The `host` and `mailbox` conditions on events may need to be extended to include regular expression matching or additional attributes (host type or protocol).

It may be useful for the triggered `action` to be not a named action but a SCRIPT file, or an inline SCRIPT, or inline Action Commands.

Make sure AKKA Actor system can start as soon as JVM starts and does not wait for an FTP connection to be made.

